@extends('template.main')

@section('title', 'Penjualan')

@section('content')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><span class="text-semibold">Penjualan</span></h4>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="{{{ url('/') }}}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Penjualan</li>
        </ul>
    </div>
</div>
<!-- /page header -->

<div class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title">List Penjualan</h5>
                    <div class="heading-elements">
                        <a href="javascript:;" class="btn btn-primary pull-right" data-toggle="modal" data-target="#modal_add">Buat Penjualan</a>
                    </div>
                </div>
    
                <table class="table my-datatable">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Pelanggan</th>
                            <th>No HP</th>
                            <th>Alamat</th>
                            <th>Barang</th>
                            <th>Jumlah</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $row)
                        <tr>
                            <td>{{{ $loop->index + 1 }}}</td>
                            <td>{{{ $row->customer_name }}}</td>
                            <td>{{{ $row->customer_phone }}}</td>
                            <td>{{{ $row->customer_address }}}</td>
                            <td>{{{ $row->barang->nama_barang }}}</td>
                            <td>{{{ $row->jumlah }}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div id="modal_add" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title">Buat Penjualan</h5>
            </div>
            {{{ Form::open(['method' => 'POST', 'action' => 'App\Http\Controllers\PenjualanController@store']) }}}
                <div class="modal-body">
                    <div class="form-group">
                        <label>Nama Pelanggan *</label>
                        <input type="text" name="name" value="{{ old('name') }}" class="form-control" placeholder="ex: Juniardy">
                    </div>
                    <div class="form-group">
                        <label>No HP *</label>
                        <input type="text" name="phone" value="{{ old('phone') }}" class="form-control" placeholder="ex: 081xxxxxx">
                    </div>
                    <div class="form-group">
                        <label>Alamat *</label>
                        <textarea class="form-control" name="address" cols="30" rows="5">{{ old('address') }}</textarea>
                    </div>
                    <div class="form-group">
                        <select name="barang" class="form-control">
                            <option value="">-- Pilih Barang --</option>
                            @foreach (@$barang as $item)
                            <option value="{{{ $item->id }}}" {{{ (old('barang', request()->get('barang')) == $item->id)? 'selected' : '' }}}>{{{ $item->nama_barang . ' - ' . $item->kode_barang }}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Jumlah *</label>
                        <input type="number" name="jumlah" value="{{ old('jumlah') }}" class="form-control" placeholder="ex: 5">
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            {{{ Form::close() }}}
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
    document.addEventListener('DOMContentLoaded', function() {
        $.extend( $.fn.dataTable.defaults, {
            autoWidth: false,
            columnDefs: [{ 
                orderable: false,
                targets: [ 5 ]
            }],
            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                searchPlaceholder: 'Type to filter...',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
            },
            drawCallback: function () {
                $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
            },
            preDrawCallback: function() {
                $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
            }
        });
        $('.my-datatable').DataTable();
    })
</script>
@endsection