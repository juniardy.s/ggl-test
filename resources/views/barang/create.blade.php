@extends('template.main')

@section('content')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><span class="text-semibold">Buat Barang</span></h4>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="{{{ url('/') }}}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Buat Barang</li>
        </ul>
    </div>
</div>
<!-- /page header -->

<div class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title">Buat Barang</h5>
                    <div class="heading-elements">
                    </div>
                </div>
                <div class="panel-body">
                    {{{ Form::open(['method' => 'POST', 'action' => 'App\Http\Controllers\BarangController@store', 'files' => true]) }}}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group form-group-material">
                                    <label class="control-label is-visible">Nama Barang *</label>
                                    <input type="text" name="name" class="form-control" value="{{{ old('name') }}}" placeholder="ex: Meja">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group form-group-material">
                                    <label class="control-label is-visible">Kode Barang</label>
                                    <input type="text" name="code" class="form-control" value="{{{ old('code') }}}" placeholder="ex: MJ12">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group form-group-material">
                                    <label class="control-label is-visible">Gambar * <small style="font-size: 11px; color: grey">Optimum Size: (512 x 512) in px</small></label>
                                    <input type="file" class="file-input-preview" name="image" data-show-caption="false" data-show-upload="false" accept="image/*" data-show-remove="true">
                                </div>
                            </div>
                        </div>
                        <div class="text-right">
                            <input type="hidden" name="back" value="{{{ @request()->back }}}">
                            <a href="{{{ (@request()->back)? request()->back : url('barang')  }}}" class="btn btn-default">Back</a>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    {{{ Form::close() }}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="{!! asset('global_assets/js/plugins/uploaders/fileinput/plugins/purify.min.js') !!}"></script>
<script src="{!! asset('global_assets/js/plugins/uploaders/fileinput/plugins/sortable.min.js') !!}"></script>
<script src="{!! asset('global_assets/js/plugins/uploaders/fileinput/fileinput.min.js') !!}"></script>
<script src="{!! asset('global_assets/js/plugins/editors/summernote/summernote.min.js') !!}"></script>
<script src="{!! asset('global_assets/js/plugins/forms/styling/uniform.min.js') !!}"></script>
<script>
    $(document).ready(function(){
        $(".form-check-input").uniform();
        var modalTemplate = '<div class="modal-dialog modal-lg" role="document">' +
        '  <div class="modal-content">' +
        '    <div class="modal-header">' +
        '      <div class="kv-zoom-actions btn-group">{toggleheader}{fullscreen}{borderless}{close}</div>' +
        '      <h6 class="modal-title">{heading} <small><span class="kv-zoom-title"></span></small></h6>' +
        '    </div>' +
        '    <div class="modal-body">' +
        '      <div class="floating-buttons btn-group"></div>' +
        '      <div class="kv-zoom-body file-zoom-content"></div>' + '{prev} {next}' +
        '    </div>' +
        '  </div>' +
        '</div>';

        // Buttons inside zoom modal
        var previewZoomButtonClasses = {
            toggleheader: 'btn btn-default btn-icon btn-xs btn-header-toggle',
            fullscreen: 'btn btn-default btn-icon btn-xs',
            borderless: 'btn btn-default btn-icon btn-xs',
            close: 'btn btn-default btn-icon btn-xs'
        };

        // Icons inside zoom modal classes
        var previewZoomButtonIcons = {
            prev: '<i class="icon-arrow-left32"></i>',
            next: '<i class="icon-arrow-right32"></i>',
            toggleheader: '<i class="icon-menu-open"></i>',
            fullscreen: '<i class="icon-screen-full"></i>',
            borderless: '<i class="icon-alignment-unalign"></i>',
            close: '<i class="icon-cross3"></i>'
        };

        // File actions
        var fileActionSettings = {
            zoomClass: 'btn btn-link btn-xs btn-icon',
            zoomIcon: '<i class="icon-zoomin3"></i>',
            dragClass: 'btn btn-link btn-xs btn-icon',
            dragIcon: '<i class="icon-three-bars" style="display: none"></i>',
            removeClass: 'btn btn-link btn-icon btn-xs',
            removeIcon: '<i class="icon-trash"></i>',
            indicatorNew: '<i class="icon-file-plus text-slate"></i>',
            indicatorSuccess: '<i class="icon-checkmark3 file-icon-large text-success"></i>',
            indicatorError: '<i class="icon-cross2 text-danger"></i>',
            indicatorLoading: '<i class="icon-spinner2 spinner text-muted"></i>'
        };
        
        $(".file-input-preview").fileinput({
            browseLabel: 'Browse',
            browseIcon: '<i class="icon-file-plus"></i>',
            uploadIcon: '<i class="icon-file-upload2"></i>',
            removeIcon: '<i class="icon-cross3"></i>',
            layoutTemplates: {
                icon: '<i class="icon-file-check"></i>',
                modal: modalTemplate
            },
            initialPreview: [
            ],
            initialPreviewConfig: [
            ],
            initialPreviewAsData: true,
            overwriteInitial: false,
            previewZoomButtonClasses: previewZoomButtonClasses,
            previewZoomButtonIcons: previewZoomButtonIcons,
            fileActionSettings: fileActionSettings
        });
    })
</script>
@endsection