@extends('template.main')
@section('title', 'Product')

@section('content')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4 style="display: inline-block"><span class="text-semibold">Barang</span></h4>
            <a href="{{{ url('barang/create') }}}" class="btn btn-primary pull-right">Buat Barang</a>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="{{{ url('admin-panel') }}}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Barang</li>
        </ul>
    </div>
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content">

    <!-- Detached content -->
    <div class="container-detached">
        <div class="content-detached">

            <!-- Grid -->
            <div class="row">
            @foreach ($data as $item)
                <div class="col-lg-4 col-xl-3 col-sm-6" style="min-height: 550px">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="thumb thumb-fixed">
                                <img src="{!! asset($item->gambar_barang) !!}" class="img img-responsive" style="width: auto; max-height: 250px" alt="">
                                <div class="caption-overflow">
                                    <span>
                                        <a href="{!! url('barang/'.base64_encode($item->id).'/edit') !!}" class="btn btn-flat border-white text-white">Edit</a>
                                        <a href="#" data-toggle="modal" data-target="#modal_delete_{!! $item->id !!}" class="btn btn-flat border-white text-white">Delete</a>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="panel-body panel-body-accent text-center">
                            <h6 class="text-semibold no-margin">
                                <a href="{!! url('barang/'.base64_encode($item->id).'?back='.request()->fullUrl()) !!}" class="text-default" style="{!! ($item->low_stock)? 'color: #c11718 !important' : '' !!}">
                                    {{ $item->nama_barang }} | {{ $item->kode_barang }}
                                </a>
                            </h6>
                        </div>
                    </div>
                </div>
                <div id="modal_delete_{{{ $item->id }}}" class="modal fade" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header bg-danger">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h5 class="modal-title">Delete Data</h5>
                            </div>
                            {{{ Form::open(['method' => 'DELETE', 'action' => ['App\Http\Controllers\BarangController@destroy', base64_encode($item->id)]]) }}}
                            <div class="modal-body">
                                Are you sure want to delete {{{ $item->name }}} ?
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-link" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-danger">Yes</button>
                            </div>
                            {{{ Form::close() }}}
                        </div>
                    </div>
                </div>
            @endforeach
            </div>
            <!-- /grid -->


            <!-- Pagination -->
            {{-- <div class="text-center content-group-lg pt-20">
                <ul class="pagination">
                    <li class="disabled"><a href="#"><i class="icon-arrow-small-left"></i></a></li>
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#"><i class="icon-arrow-small-right"></i></a></li>
                </ul>
            </div> --}}
            {{ $data->links() }}
            <!-- /pagination -->

        </div>
    </div>
    <!-- /detached content -->

</div>
<!-- /content area -->
@endsection

@section('js')
<script>
    document.addEventListener('DOMContentLoaded', function() {
        // Styled checkboxes, radios
        $('.styled').uniform();
    });
</script>
@endsection