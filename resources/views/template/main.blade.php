
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>@yield('title')</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="{{ asset('global_assets/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('assets/css/core.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('assets/css/components.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('assets/css/colors.min.css') }}" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script src="{{ asset('global_assets/js/plugins/loaders/pace.min.js') }}"></script>
	<script src="{{ asset('global_assets/js/core/libraries/jquery.min.js') }}"></script>
	<script src="{{ asset('global_assets/js/core/libraries/bootstrap.min.js') }}"></script>
	<script src="{{ asset('global_assets/js/plugins/loaders/blockui.min.js') }}"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script src="{{{ asset('global_assets/js/plugins/tables/datatables/datatables.min.js') }}}"></script>
	<script src="{{{ asset('global_assets/js/plugins/forms/selects/select2.min.js') }}}"></script>
	<script src="{{{ asset('global_assets/js/demo_pages/datatables_advanced.js') }}}"></script>
	<script src="{{{ asset('global_assets/js/plugins/forms/styling/uniform.min.js') }}}"></script>
	<script src="{{{ asset('global_assets/js/plugins/forms/styling/switch.min.js') }}}"></script>
	<script src="{{{ asset('global_assets/js/plugins/editors/summernote/summernote.min.js') }}}"></script>

	<script src="{{ asset('assets/js/app.js') }}"></script>
	<!-- /theme JS files -->

	@yield('css')

</head>

<body class="@yield('body-class')">

	<!-- Main navbar -->
	@include('template.navbar')
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			@include('template.sidebar')
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">
				@yield('content')
			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>

@include('sweetalert::alert', ['cdn' => "https://cdn.jsdelivr.net/npm/sweetalert2@9"])

@yield('js')

</html>
