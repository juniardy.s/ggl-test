<div class="sidebar sidebar-main">
    <div class="sidebar-content">

        <!-- User menu -->
        <div class="sidebar-user">
            <div class="category-content">
                <div class="media">
                    <a href="#" class="media-left"><img src="{{ asset('logo.png') }}" class="img-circle img-sm" alt=""></a>
                    <div class="media-body">
                        <span class="media-heading text-semibold">Juniardy Setiowidayoga</span>
                        <div class="text-size-mini text-muted">
                            <i class="icon-pin text-size-small"></i> &nbsp;Surabaya
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /user menu -->


        <!-- Main navigation -->
        <div class="sidebar-category sidebar-category-visible">
            <div class="category-content no-padding">
                <ul class="navigation navigation-main navigation-accordion">

                    <!-- Main -->
                    <li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
                    <li class="{{{ (request()->is('barang') || request()->is('barang/*'))? 'active' : ''  }}}"><a href="{{ url('barang') }}"><i class="icon-box"></i> <span>Barang</span></a></li>
                    <li class="{{{ (request()->is('pembelian') || request()->is('penjualan'))? 'active' : ''  }}}">
                        <a href="#"><i class="icon-stack2"></i> <span>Transaksi</span></a>
                        <ul>
                            <li class="{{{ (request()->is('pembelian'))? 'active' : ''  }}}"><a href="{{ url('pembelian') }}">Pembelian</a></li>
                            <li class="{{{ (request()->is('penjualan'))? 'active' : ''  }}}"><a href="{{ url('penjualan') }}">Penjualan</a></li>
                        </ul>
                    </li>

                </ul>
            </div>
        </div>
        <!-- /main navigation -->

    </div>
</div>