<?php

namespace App\Models;

use App\Models\Barang;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Pembelian extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function barang()
    {
        return $this->hasOne(Barang::class, 'id', 'id_barang');
    }
}
