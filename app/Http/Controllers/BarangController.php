<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use Alert;
use App\Models\Barang;
use Illuminate\Http\Request;
use Carbon\Carbon;

class BarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = Barang::paginate(10);
        return view('barang.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('barang.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $valid = Validator::make($request->all(), [
            'image' => 'required|image|mimes:jpg,jpeg,png',
            'name' => 'required',
            'code' => 'required'
        ]);

        if ($valid->fails()) {
            Alert::info($valid->errors()->all()[0]);
            return redirect()->back()->withInput();
        } else {
            $post = DB::transaction(function () use ($request) {
                try {
                    $img = $this->saveImg($request->file('image'));

                    $barang = Barang::create([
                        'nama_barang' => $request->name,
                        'kode_barang' => $request->code,
                        'gambar_barang' => @$img['location']
                    ]);

                    if ($barang) {
                        Alert::success('Barang berhasil dibuat!');
                    } else {
                        throw new \Exception('Gagal membuat barang!');
                    }
                    return redirect('barang');
                } catch (\Exception $e) {
                    DB::rollback();
                    Alert::error($e->getMessage());
                    return redirect()->back()->withInput();
                }
            });

            return $post;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = Barang::find(base64_decode($id));
        if (!$data) abort(403);
        return view('barang.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $barang = Barang::find(base64_decode($id));

        $barang->delete();
        
        Alert::success('Barang telah dihapus!');
        return redirect()->back();
    }

    protected function saveImg($img)
    {
        $destinationPath = 'img';
        $subdestinationPath = 'barang';
        $extension = $img->getClientOriginalExtension();
        $fileName = sha1(time().rand()).'.'.$extension;
        $img->move($destinationPath. '/' . $subdestinationPath , $fileName);
        $data['location'] = $destinationPath. '/' . $subdestinationPath . '/' . $fileName;
        $data['filename'] = $img->getClientOriginalName();
        $data['storage_filename'] = $fileName;
        return $data;
    }
}
