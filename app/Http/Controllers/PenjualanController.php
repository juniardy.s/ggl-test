<?php

namespace App\Http\Controllers;

use DB;
use Alert;
use Validator;
use Carbon\Carbon;
use App\Models\Stok;
use App\Models\Barang;
use App\Models\Penjualan;
use Illuminate\Http\Request;

class PenjualanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $barang = Barang::all();
        $data = Penjualan::orderBy('id', 'desc')->get();
        return view('penjualan', compact('data', 'barang'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $valid = Validator::make($request->all(), [
            'name' => 'required',
            'barang' => 'required',
            'jumlah' => 'required',
        ]);

        if ($valid->fails()) {
            Alert::info($valid->errors()->all()[0]);
            return redirect()->back()->withInput();
        } else {
            $post = DB::transaction(function () use ($request) {
                try {
                    $penjualan = Penjualan::create([
                        'id_barang' => $request->barang,
                        'customer_name' => $request->name,
                        'customer_phone' => $request->phone,
                        'customer_address' => $request->address,
                        'jumlah' => $request->jumlah
                    ]);
                    if ($penjualan) {
                        $stok = Stok::create([
                            'id_barang' => $request->barang,
                            'total_barang' => $request->jumlah,
                            'jenis_stok' => 'out'
                        ]);
                        Alert::success('Penjualan berhasil dibuat!');
                    } else {
                        throw new \Exception('Gagal membuat Penjualan!');
                    }
                    return redirect()->back();
                } catch (\Exception $e) {
                    DB::rollback();
                    Alert::error($e->getMessage());
                    return redirect()->back()->withInput();
                }
            });

            return $post;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
